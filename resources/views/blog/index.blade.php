
@extends('base')

@section('title', "Accueil du blog")

@section('content')
<h1>Mon blog</h1>
@foreach($posts as $p)

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{$p->title}}</h5>
            <p class="card-text">{{substr($p->content, 0, 10) . '...'}}</p>
            <a href="{{route("blog.show", ["post" => $p->slug])}}">
                <button type="button" class="btn btn-primary">Lire la suite</button>
            </a>

            @if($p->category || !$p->tags->isEmpty())
            <div class="card-footer">
            @if($p->category)
                <p>Catégorie: <strong>{{$p->category->name}}</strong></p>
            @endif

                  @if(!$p->tags->isEmpty())
                      Tags:
                      @foreach($p->tags as $tag)
                        <span class="badge bg-secondary">{{$tag->name}}</span>
                    @endforeach
                @endif
            </div>
            @endif
        </div>
    </div>

@endforeach
{{$posts->links()}}
@endsection
