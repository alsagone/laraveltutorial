@if ($post->id)
    <h1>Édition d'un article</h1>
@else
    <h1>Création d'un article</h1>
@endif

<form action="" method="post" class="vstack gap-2">
    @csrf
    <div class="form-group">
        <label for="title">Titre</label>
        <input id="title" type="text" name="title" value="{{@old('title', $post->title)}}" class="form-control"></input>
        @error('title')
        {{$message}}
        @enderror
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input id="slug" type="text" name="slug" value="{{@old('slug', $post->slug)}}" class="form-control"></input>
        @error('slug')
        {{$message}}
        @enderror
    </div>

    <div class="form-group">
        <label for="content">Contenu</label>
        <textarea id="content" name="content" type="text" class="form-control">{{@old('content', $post->content)}}</textarea>
        @error('content')
        {{$message}}
        @enderror
    </div>

    <div class="form-group">
        <label for="category">Catégorie</label>
        <select id="category" name="category_id" type="text" class="form-control">
            <option value="" disabled selected>Sélectionnez une catégorie</option>
            @foreach($categories as $category)
                <option @selected(old('category_id', $post->category_id) === $category->id) value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
        @error('category_id')
        {{$message}}
        @enderror
    </div>

    @php
    $tagIds = $post->tags()->pluck('id');
    @endphp
    <div class="form-group">
        <label for="tag">Tags</label>
        <select id="tag" name="tags[]" type="text" class="form-control" multiple>
            <option value="" disabled selected>Sélectionnez un ou plusieurs tags</option>
            @foreach($tags as $tag)
                <option @selected($tagIds->contains($tag->id)) value="{{$tag->id}}">{{$tag->name}}</option>
            @endforeach
        </select>
        @error('tags')
        {{$message}}
        @enderror
    </div>

    <?php // If the post ID is defined, this means the user is currently editing a post ?>
@if ($post->id)
        <button type="submit" class="btn btn-success">Valider les modifications</button>
    @else
        <button type="submit" class="btn btn-primary">Créer un article</button>
    @endif

</form>
