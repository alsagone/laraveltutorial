@extends('base')

@section('title', $post->title)

@section('content')
    <h1>{{$post->title}}</h1>
    <p>{{$post->content}}</p>
    <a href="{{route("blog.edit", ["post" => $post])}}">
        <button type="button" class="btn btn-primary">Editer</button>
    </a>
@endsection;
