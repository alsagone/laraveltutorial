@extends("base")

@section("title", "Édition d'un article")

@section('content')
    @include('blog.form')
@endsection
