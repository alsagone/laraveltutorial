<?php
$routeName = request()->route()->getName()
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a @class(['nav-link', 'active' =>  $routeName === "blog.index" ]) href="{{route('blog.index')}}">Blog</a>
                <a @class(['nav-link', 'active' =>  $routeName === "blog.create" ]) href="{{route('blog.create')}}">Nouvel article</a>
            </div>
        </div>
    </div></nav>
