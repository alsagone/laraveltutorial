Débug d'une variable

```php
$posts = [];
dd($posts);
```

## Routes

### Avec une vue

On utilise

```php
Route::get('/chemin', function() {
    return view('nom_de_la_vue');
};)
```

### Récupérer les paramètres

Via l'objet Request (équivalent de \$\_GET/\$\_POST)

Importer `use Illuminate\Http\Request;`
Exemple: `localhost:8000/blog?name=john`

```php
Route:get('/blog', function(Request $request) {
    return [
        "name" => $request->input("name"),
        "article" => "Article 1"
    ];
});

```

Utiliser la méthode input fait que la valeur est null si jamais le paramètre n'est pas fourni dans l'URL (et ne déclenche pas d'erreur)

On peut fournir une valeur par défaut

```php
Route:get('/blog', function(Request $request) {
    return [
        "name" => $request->input("name", "John Doe"),
        "article" => "Article 1"
    ];
});
```

### Routes dynamiques

Exemple: localhost:8000/blog/mon-premier-article-12

Paramètres de la route entre acollades, puis on les utilise dans la fonction
Ensuite, on peut utiliser des Regex pour filtrer correctement

```php
Route::get('/blog/{slug}-{id}', function(string $slug, string $id) {
    return [
        "slug" => $slug,
        "id" => $id
    ];
})->where([
    "id" => '\d*',
    "slug" => "[a-z0-9\-]*"
]);
```

### Routes nommées

Dans l'exemple du blog, on voudrait par exemple rediriger l'user vers l'article correspondant à l'URL.
Pour ça, on peut déjà nommer des routes avec la méthode `name`

Ça nous permet de générer dynamiquement une route avec la méthode \route(nom_route, [parametres_optionnels])

```php
Route::get('/blog', function (Request $request) {
    return [
        "link" => \route("blog.show", ["slug" => "article", "id" => 13])
    ];
})->name("blog.index");

Route::get('/blog/{slug}/{id}', function(string $slug, string $id, Request $request) {
    return [
        "slug" => $slug,
        "id" => $id,
        "name" => $request->input("name", "John Doe")
    ];
})->where([
    "id" => '\d*',
    "slug" => "[a-z0-9\-]*"
])->name("blog.show");
```

Maintenant en se rendant sur "/blog", on reçoit le lien `http://localhost:8000/blog/article-13`

L'avantage, c'est que si on change le format des URLs dans la route "blog.show", automatiquement le format du lien renvoyé par la route "blog.index" change aussi

Pour lister toutes les routes disponibles
`php artisan route:list`

### Regrouper des routes

Dans notre exemple, nos deux routes ont le même préfixe "/blog/" donc il serait intéressant de les regrouper

```php
Route::prefix("/blog")->name("blog.")->group(function() {
    Route::get('/', function (Request $request) {
        return [
            "link" => \route("show", ["slug" => "article", "id" => 13])
        ];
    })->name("index");

    Route::get('/{slug}/{id}', function(string $slug, string $id, Request $request) {
        return [
            "slug" => $slug,
            "id" => $id,
            "name" => $request->input("name", "John Doe")
        ];
    })->where([
        "id" => '\d*',
        "slug" => "[a-z0-9\-]*"
    ])->name("show");
});
```

On regroupe nos deux routes sous le préfixe "/blog" et ensuite dans la fonction, on n'a plus besoin d'appeler "/blog" à chaque fois, "/" suffit

Pareil pour les noms de route, plus besoin d'appeler "blog."

## Interaction avec une base de données

Pas besoin de remplir la table "à la main", on peut faire ça directement

### Créer une migration

`php artisan make:migration nomDeLaMigration`

Permet de créer une migration qui va nous permettre de rajouter des infos dans la BDD

Fichier de migration crée dans le dossier `database/migrations` qui contient deux méthodes:

-   `up()` qui permet d'expliquer comment générer les données correspondant à cette migration
-   down() destruction des données crées

Du coup, ça permet de créer des tables en interagissant avec une API PHP plutôt que de faire du SQL

### Insérer des colonnes dans la table

```php
public function up(): void
    {
        Schema::create('post', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("title");
            $table->string("slug")->unique();
            $table->longText("content");
        });
    }
```

Une fois fait, `php artisan migrate`

### Modèles

`php artisan make:model Post`
On peut créer les modèles ET les migrations en ajoutant un -m

Automatiquement, ça va créer un dossier "Models" dans "app/"

#### Insertion (naïve)

Dans les routes

```php
Route::prefix("/blog")->name("blog.")->group(function() {
    Route::get('/', function (Request $request) {
        $post = new \App\Models\Post();
        $post->title = "Mon premier article";
        $post->slug = "mon-premier-article";
        $post->content = "Mon contenu";
        $post->save();
        return $post;
```

Ça rajoute automatiquement des attributs id, updated_at, created_at (timestamps)

Si on réactualise la page, Laravel essaie de ré-insérer l'objet
On a une erreur parce que la colonne slug est paramétrée pour être unique

#### Récupérer des articles

```php
Route::prefix("/blog")->name("blog.")->group(function() {
    Route::get('/', function (Request $request) {
        // Tous les posts
        $posts = \App\Models\Post::all();

        //Champs spécifiques
        return \App\Models\Post::all(["id", "title"]);

        //Récupérer seulement le premier item d'une collection
        $posts->first();

        //Trouver par ID; null si pas présent
        $p = \App\Models\Post::find(1);

        // Renvoyer une erreur si pas présent (ici page 404)
        $p = \App\Models\Post::findOrFail(245);

        // Pagination(nb_items_par_page)
        $pag = \App\Models\Post::paginate(1, ["id", "title"]);

        // Système de Query Builder
        $q = \App\Models\Post::where('id', '>', 0)->limit(1)->get();
    }
});
```

#### Modifier/Supprimer des articles

```php
$p = \App\Models\Post::findOrFail(1);
$p->title = "Nouveau titre";
$p->save();

//Suppression
$p = \App\Models\Post::findOrFail(2);
$p->delete();

```

#### Insertion

```php
Route::get('/creer-article', function (Request $request) {
        $p = \App\Models\Post::create([
            "title" => "Mon nouvel article",
            "slug" => "mon-nouvel-article",
            "content" => "Contenu de mon nouvel article"
        ]);
        $p->save();
        return $p;
    });

```

Tel quel, ce bout de code renvoie une erreur "`(add [title]) to fillable property to allow mass assignment on [App\Models\Post]`
-> parce que sécurité par défaut dans Laravel

Il faut indiquer quelles propriétés sont assignables de la sorte dans le modèle Post

```php
class Post extends Model
{
    use HasFactory;
    protected $fillable = ["title", "slug", "content"];

    // Équivalent inverse avec $guarded pour indiquer les champs protégés
}
```

On peut alors changer notre route "blog.show"

```php
Route::get('/{slug}/{id}', function(string $slug, string $id, Request $request) {
        $p = \App\Models\Post::findOrFail($id);

        //Si le slug passé en paramètre n'est pas le bon, rediriger vers la route avec les bons paramètres
        return $p->slug === $slug
            ? $p
            : to_route('blog.show', ["slug" => $p->slug, "id" => $id]);
    })->where([
        "id" => '\d*',
        "slug" => "[a-z0-9\-]*"
    ])->name("show");
```

### Controllers

`php artisan make:controller BlogController`
-> crée un fichier `app/Http/Controllers/BlogController.php`
Ils s'occupent de la logique de l'application pour éviter de faire ça dans les routes

```php
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class BlogController extends Controller
{
    // Homepage for our posts
    public function index(): LengthAwarePaginator {
        return Post::paginate(25);
    }

    public function show(string $slug, string $id): Post | RedirectResponse {
        $p = Post::findOrFail($id);

        // If the provided slug is different from the post's, redirect to the correct route with the correct slug
        return $p->slug === $slug
            ? $p
            : to_route('blog.show', ["slug" => $p->slug, "id" => $id]);
    }
}
```

Et du coup, on peut faire appel au controller dans la route

```php
Route::prefix("/blog")->name("blog.")->group(function() {
    Route::get('/', [BlogController::class, 'index'])->name("index");

    Route::get('/{slug}/{id}', [BlogController::class, 'show'])->where([
        "id" => '\d*',
        "slug" => "[a-z0-9\-]*"
    ])->name("show");
});
// ...
```

## Blade et templates

### section/yield/extends

#### Fichier base.blade.php

```php
<html>
<head>
    <title>@yield("title")</title>
</head>
<body>@yield("content")</body>
</html>
```

#### Fichier index.blade.php

```php
@extends("base")

@section("content")
<h1>Mon blog</h1>
@endsection

@section("title", "Accueil du blog")
```

#### Directives

```php
@if(true)
    {{"Oui c'est vrai"}}
@else
    {{"Non c'est faux"}}
@endif
```

#### Affichage des articles

Lorsqu'on fait appel à une vue dans un Controller, on peut lui transmettre des paramètres via un tableau associatif

```php
// BlogController
public function index(): View {
    return view('blog.index', ["posts" => Post::paginate(25)]);
}

//On peut y accéder directement dans la index.blade.php via la variable $posts

@section('content')
<h1>Mon blog</h1>
@foreach($posts as $p)
    <article>
        <h2>{{$p->title}}</h2>
        <p>{{$p->content}}</p>
        <p><a href="{{route("blog.show", ["slug" => $p->slug, "id" => $p->id])}}">Lien</a> </p>
    </article>
@endforeach
// Pagination
    {{$posts->links()}}
@endsection;
```

##### Affichage d'un article en particulier

```php
//BlogController
public function show(string $slug, string $id): View | RedirectResponse {
        $p = Post::findOrFail($id);

        // If the provided slug is different from the post's, redirect to the correct route with the correct slug
        return $p->slug === $slug
            ? view("blog.show", ["post" => $p])
            : to_route('blog.show', ["slug" => $p->slug, "id" => $id]);
    }

//show.blade.php
@extends('base')

@section('title', $post->title)

@section('content')
    <h1>{{$post->title}}</h1>
    <p>{{$post->content}}</p>
@endsection;
```

#### Classe conditionnelle

```php
$routeName = request()->route()->getName();

<li @class(['nav-item', 'active' =>  str_starts_with($routeName, "blog.") ])>
```

#### Réutilisation d'un component

-   Créer le component dans le dossier `views`: component.blade.php
-   @include ("component") dans le fichier dans lequel l'importer

## Model binding

Dans la route Show, on récupère l'article puis on continue l'exécution en affichant son contenu
Mais Laravel peut pre-fetch ça plus rapidement

```php
public function show(string $slug, Post $post): View | RedirectResponse {


        // If the provided slug is different from the post's, redirect to the correct route with the correct slug
        return $post->slug === $slug
            ? view("blog.show", ["post" => $post])
            : to_route('blog.show', ["slug" => $post->slug, "id" => $post]);
    }
```

Du coup en mettant directement le post dans les paramètres de la route, quand on visite blog/1, Laravel comprend directement qu'il doit aller chercher l'article d'ID 1 dans la database

## Formulaires 

On crée notre formulaire dans une page à part et on lui crée une route Get pour y accéder

```php 
// create.blade.php
@extends("base")

@section("title", "Création d'un article")

@section('content')
    <form action="" method="post">
    @csrf
        <label for="title">Titre</label>
            Titre
            <input id="title" type="text" name="title" value="Article de démonstration"></input>

        <label for="content">Contenu</label>

            <textarea id="content" name="content" type="text" value="Contenu de démonstration"></textarea>

        <button>Enregistrer</button>
    </form>
@endsection
```



En l'état des choses, on n'a pas encore géré le fait que le formulaire fasse la requête POST.
On ajoute une nouvelle route

```php 
Route::post('/new', 'store')->name('store');
```

Et maintenant dans le `BlogController`, on peut créer la fonction `store`.

```php
public function store(Request $request) : RedirectResponse {
        $post = Post::create(
            [
                'title' => $request->input('title'),
                'content' => $request->input('content'),
                'slug' => Str::slug($request->input('title'))
            ]
        );

        return redirect(route('blog.show',
            ['post' => $post]))
            ->with("success", "L'article a bien été sauvegardé");
    }
```

Le `redirect` peut être agrémenté d'un `with` qui va sauvegarder une clé/valeur dans la session. Ici, on peut indiquer à Laravel que l'article a bien été ajouté et afficher ce message dans la page après redirection.

Il suffit de rajouter ça dans le `base.blade.php`

```php 
@if (session('success'))
	<div class="alert alert-success">
		{{session('success')}}
    </div>
@endif
```

### Validation des données 

`php artisan make:request CreatePostRequest`

`Request [app/Http/Requests/CreatePostRequest.php] created successfully.`

Dans ce fichier, on peut mettre des contraintes de validation.

D'abord, dans la méthode `authorize`, changer le `false` qui est là par défaut en `true` pour autoriser l'utilisateur à faire cette requête.

Dans la méthode `rules`, on peut écrire des règles de validation.

```php
public function rules(): array
    {
        return [
            'title' => ['required', 'min:8'],
            'slug' => ['required', 'min:8', 'regex:/^[\da-z\-]*$/'],
            'content' => ['required']
        ];
    }
```

À ça, on peut utiliser la méthode `prepareForValidation` qui s'exécute avant la vérification des règles.
Ici, si la requête ne contient pas de champ 'slug', on peut l'ajoute.

```php
public function prepareForValidation()
    {
        $this->merge(['slug' => $this->input('slug') ?: Str::slug($this->input('title'))]);
    }
```

PS: L'opérateur `?:` (aka l'Elvis operator) permet de réduire

```php
$prenom = $this->prenom ? $this->prenom : "Hakim";
```

en 

```php
$prenom = $this->prenom ?: "Hakim";
```

Maintenant dans le BlogController, plutôt que d'utiliser un objet Request, on peut utiliser notre CreatePostRequest

```php
public function store(CreatePostRequest $request) : RedirectResponse {
        $post = Post::create($request->validated());

        return redirect(route('blog.show',
            ['post' => $post]))
            ->with("success", "L'article a bien été sauvegardé");
    }
```

La différence principale avec avant, c'est qu'au lieu d'avoir un tableau associatif avec nos données, on utilise directement la méthode `validated()` qui renvoie un tableau avec toutes les données qui passent les règles de validation.

Et du coup, si des données sont incorrectes, elles seront manquantes dans les données qui sont envoyées et ça provoquera un message d'erreur.

Pour les afficher, on utilise la directive `@error`

```php 
<form action="" method="post">
        @csrf
        <div class="input">
        <label for="title">Titre</label>
            Titre

            <input id="title" type="text" name="title" value="Article de démonstration"></input>
            @error('title')
                {{$message}}
            @enderror
        </div>

        <div class="input">
        <label for="content">Contenu</label>

            <textarea id="content" name="content" type="text" value="Contenu de démonstration"></textarea>
            @error('content')
            {{$message}}
            @enderror
        </div>
        <button>Enregistrer</button>
    </form>
```

Une des améliorations possibles de ce formulaire est qu'en l'état actuel, s'il y a une erreur, Laravel ne récupère pas l'ancienne valeur que l'utilisateur a envoyé.
Pour ce faire, on peut utiliser la directive `@old(nomDuChamp, 'valeur_par_defaut');`

### Éditer un post

 On commence par créer deux nouvelles routes et les fichiers qui sont associés.

```php
// edit.blade.php 
@extends("base")

@section("title", "Édition d'un article")

@section('content')
    <form action="" method="post">
        @csrf
        <div class="input">
        <label for="title">Titre</label>
            <input id="title" type="text" name="title" value="{{@old('title', $post->title)}}"></input>
            @error('title')
                {{$message}}
            @enderror
        </div>

        <div class="input">
            <label for="slug">Slug</label>
            <input id="slug" type="text" name="slug" value="{{@old('slug', $post->slug)}}"></input>
            @error('slug')
            {{$message}}
            @enderror
        </div>

        <div class="input">
        <label for="content">Contenu</label>
            <textarea id="content" name="content" type="text">{{@old('content', $post->content)}}</textarea>
            @error('content')
            {{$message}}
            @enderror
        </div>
        <button>Enregistrer</button>
    </form>
@endsection
```

Pour récupérer le contenu du post et l'injecter dans le formulaire, on peut mettre `$post->title/slug/content` en tant que valeur par défaut dans les `@old(...)`

Pour s'assurer que le *slug* est unique, on peut utiliser la règle 

```php
Rule::unique('posts')->ignore($this->route()->parameter('post'))
```

Qui s'assure que le slug est unique MAIS qui ignore cette règle si elle vient du paramètre `post` ( = un post qui existe déjà)

Pour éviter de répéter deux fois le formulaire dans les fichiers `create/edit.blade.php`, on peut créer un fichier `form.blade.php` où on va stocker tout le formulaire 

Et dans les fichiers `create/edit.blade.php`, on peut tout simplement l'inclure.

```php
// edit.blade.php
@extends("base")

@section("title", "Création d'un article")

@section('content')
    @include('blog.form')
@endsection
    
//form.blade.php
    <form>...</form>
```

On prend la version "édition" du formulaire donc ça fait référence à la variable `$post`.

Ce qui va poser problème dans le cas où on veut créer un nouvel article. Pour pallier à ça, dans le BlogController, on peut initialiser et passer à la vue `blog.create` un Post vide.

```php
public function create(): View {
    $post = new Post();
    return view("blog.create", ["post" => $post]);
}
```

On peut aussi changer le bouton de validation selon si on est en mode création ou édition d'un article

```php
<?php // If the post ID is defined, this means the user is currently editing a post ?>
    
@if ($post->id)
        <button type="button" class="btn btn-success">Valider les modifications</button>
    @else
        <button type="button" class="btn btn-primary">Créer un article</button>
    @endif
```

## Relations

### Catégories pour un article 

#### Setup initial

`php artisan make:migration Category -m`
Dans la migration

```php
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            
            // Name of the category
            $table->string('name');
        });

        // Add a foreign key on the posts' table referencing a category
        Schema::table('posts', function (Blueprint $table) {
            $table->foreignIdFor(Category::class)->nullable()->constrained()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeignIdFor(Category::class);
        });
    }
};
```

`php artisan migrate && php artisan ide-helper:models -M`

#### Créer des catégories

* On set le nom de nos catégories comme `fillable`
  ```php
  class Category extends Model
  {
      use HasFactory;
  
      protected $fillable = ["name"];
  }
  ```

* Ensuite, on peut créer des catégories depuis le blogController 

  ```php
  public function index(): View {
          Category::create(
              ["name" => "Categorie 1"]
          );
  
          Category::create(
              ["name" => "Categorie 2"]
          );
          
          return view('blog.index', ["posts" => Post::paginate(1)]);
      }
  ```

* 
