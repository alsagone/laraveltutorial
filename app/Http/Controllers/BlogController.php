<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormPostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    // Homepage for our posts
    public function index(): View {
        return view('blog.index', ["posts" => Post::with(['category', 'tags'])->paginate(3)]);
    }

    public function show(Post $post): View {
        return view("blog.show", ["post" => $post]);
    }

    public function create(): View {
        $post = new Post();
        return view("blog.create", ["post" => $post,
            "categories" => Category::select("id", "name")->get(),
            "tags" => Tag::select("id", "name")->get()]);
    }

    public function store(FormPostRequest $request) : RedirectResponse {
        $post = Post::create($request->validated());

        return redirect(route('blog.show',
            ['post' => $post]))
            ->with("success", "L'article a bien été sauvegardé");
    }

    public function edit (Post $post) {
        return view("blog.edit", ["post" => $post,
            "categories" => Category::select("id", "name")->get(),
            "tags" => Tag::select("id", "name")->get()]);
    }

    public function update(Post $post, FormPostRequest $request)  {
        $post->update($request->validated());
        $post->tags()->sync($request->validated('tags'));
        return redirect(route('blog.show',
            ['post' => $post]))
            ->with("success", "L'article a bien été edité");
    }
}
